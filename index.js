const http= require("http");
const port= 3000
const server= http.createServer((req, resp)=>{
	if(req.url == '/login'){
		resp.writeHead(200, {'Content-Type': 'text/plain'})
		resp.end('Welcome to the login page.')
	} else {
		resp.writeHead(404, {'Content-Type': 'text/plain'})
		resp.end("I'm sorry the page you are looking for cannot be found.")
	}
})

server.listen(port)

console.log(`Server now accessible at localhost:${port}.`)